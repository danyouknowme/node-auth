import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import User from '../models/User.js';

const router = express.Router();

router.post('/register', async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: `${req.body.firstname} ${req.body.lastname}`,
        username: req.body.username,
        password: hashedPassword
    });

    const result = await user.save();
    const { password, ...data } = await result.toJSON();
    res.status(200).json(data);
});

router.post('/login', async (req, res) => {
    const user = await User.findOne({ username: req.body.username });

    // check user by username
    (!user) && res.status(404).json({ message: 'user not found' });
    // check password is match to the registered password
    !await bcrypt.compare(req.body.password, user.password) && res.status(400).json({ message: 'invalid credentails' });

    const token = jwt.sign({ _id: user._id }, 'secret');

    res.cookie('jwt', token, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000 // a day
    });
    
    res.send({
        message: 'login success'
    });
    
});

router.get('/user', async (req, res) => {
    try {
        const cookie = req.cookies['jwt'];

        const claims = jwt.verify(cookie, 'secret');

        !claims && res.status(401).json({ message: 'unauthenticated' });

        const user = await User.findOne({ id: claims._id });
        const { password, ...data } = await user.toJSON();

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.post('/logout', (req, res) => {
    res.cookie('jwt', '', { maxAge: 0 });
    res.status(200).json({ message: 'logout success' });
});

export { router };
