import { Switch, Route } from 'react-router-dom';
import { Homepage, Register, Login } from './pages/pages'; 

const App = () => {
	return (
		<Switch>
			<Route exact path='/'>
				<Homepage />
			</Route>
			<Route path='/register'>
				<Register />
			</Route>
			<Route path='/login'>
				<Login />
			</Route>
		</Switch>
	);
}

export default App;
