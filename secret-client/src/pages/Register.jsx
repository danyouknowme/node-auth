import React, { useState } from 'react';
import axios from 'axios';

const Register = () => {
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	const registerForm = async () => {
		axios.post('http://localhost:8000/api/register', {
			firstname: firstname,
			lastname: lastname,
			username: username,
			password: password,
		}).then(() => {
			console.log('successfully registered');
		}).catch((error) => {
			console.error(error.message);
		})
	}

	return (
		<div className="register">
			<input type="text" placeholder="First Name" onChange={(e) => setFirstname(e.target.value)} />
			<input type="text" placeholder="Last Name" onChange={(e) => setLastname(e.target.value)} />
			<input type="text" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
			<input type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
			<input type="password" placeholder="Confirm Password" onChange={(e) => setConfirmPassword(e.target.value)} style={{borderColor: `${password !== confirmPassword ? 'red': ''}`}}/>
			{password === confirmPassword ? (
				<button onClick={registerForm}>Register</button>
			): (
				<button disabled>Register</button>
			)}
		</div>
	);
};

export default Register;