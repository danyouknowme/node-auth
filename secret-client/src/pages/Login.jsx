import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

const Login = () => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const loginForm = () => {
		axios.post('http://localhost:8000/api/login', {
			username: username,
			password: password,
		})
			.then((response) => {
				console.log(response.data);
			})
			.catch((error) => console.error(error));
		<Redirect to="/" />
		// }).then(() => {
		// 	console.log('Login successful');
		// }).catch((error) => {
		// 	console.error(error);
		// });
	}

	return (
		<div className="login">
			<input type="text" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
			<input type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
			<button onClick={loginForm}>Login</button>
		</div>
	);
};

export default Login;