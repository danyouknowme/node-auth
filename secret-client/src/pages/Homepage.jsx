import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Homepage = () => {
	const [user, setUser] = useState(null);

	useEffect(() => {
		axios.get('http://localhost:8000/api/user')
			.then((response) => console.log(response.data))
			.catch((error) => console.error(error));
	})

	return (
		<div>
			<h1>Hello</h1>
		</div>
	);
};

export default Homepage;
