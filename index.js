import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import cookieParser from 'cookie-parser';

const app = express();
import { router as authRoutes } from './routes/auth.js';

mongoose.connect('mongodb://localhost/node-auth', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Connected to database successfully'))
    .catch(error => console.error(error));

app.use(express.json());
app.use(cors());
app.use(cookieParser());

app.use('/api', authRoutes);

app.listen(8000, () => {
    console.log('Server listening on port 8000');
});